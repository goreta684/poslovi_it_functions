const functions = require("firebase-functions");
const app = require("express")();
const FBAuth = require("./util/fbAuth");
const { db } = require("./util/admin");

// Added 29.5. // Maybe don't need this
/*var allowCrossDomain = function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
};
app.use(allowCrossDomain);*/

const cors = require("cors");
app.use(cors());

const {
  getAllCards,
  postOneCard,
  getCard,
  commentOnCard,
  likeCard,
  unlikeCard,
  deleteCard,
  deleteComment,
  editPost,
  editComment,
} = require("./handlers/cards");
const {
  signup,
  login,
  uploadImage,
  addUserDetails,
  getAuthenticatedUser,
  getUserDetails,
  markNotificationsRead,
  getAllUsers,
  getUsersByRole,
  getUsersByLocation,
  getUsersByEmail,
  followUser,
  unfollowUser,
  getAllFollowers,
  getAllFollowing,
  getUsersByTech,
} = require("./handlers/users");

// Cards routes
app.get("/cards", getAllCards);
app.post("/card", FBAuth, postOneCard);
app.get("/card/:cardId", getCard);
app.delete("/card/:cardId", FBAuth, deleteCard);
app.delete("/cm/:cardId/:commentId", FBAuth, deleteComment);
app.get("/card/:cardId/like", FBAuth, likeCard);
app.get("/card/:cardId/unlike", FBAuth, unlikeCard);
app.post("/card/:cardId/comment", FBAuth, commentOnCard);
app.put("/card/:cardId", FBAuth, editPost);
app.put("/cm/:commentId", FBAuth, editComment);

// Users routes
app.post("/signup", signup);
app.post("/login", login);
app.post("/user/image", FBAuth, uploadImage);
app.post("/user", FBAuth, addUserDetails);
app.get("/user", FBAuth, getAuthenticatedUser);
app.get("/user/:username", getUserDetails);
app.post("/notifications", FBAuth, markNotificationsRead);
app.get("/users/:role", getUsersByRole);
app.get("/users", getAllUsers);
app.get("/locations/:location", getUsersByLocation);
app.get("/emails/:email", getUsersByEmail);
app.get("/tech/:techname", FBAuth, getUsersByTech);
//Follow
app.get("/user/:username/follow", FBAuth, followUser);
app.get("/user/:username/unfollow", FBAuth, unfollowUser);
app.get("/followers/:recipient", FBAuth, getAllFollowers); // Delete /:recipient and in users ->
app.get("/following/:sender", FBAuth, getAllFollowing);

exports.api = functions.region("europe-west1").https.onRequest(app);

// Delete Notification On Delete Comment
exports.deleteNotificationOnDeleteComment = functions
  .region("europe-west1")
  .firestore.document("comments/{id}")
  .onDelete((snapshot) => {
    return db
      .doc(`/notifications/${snapshot.id}`)
      .delete()
      .catch((err) => {
        console.error(err);
        return;
      });
  });

// Delete Notification On UnLike
exports.deleteNotificationOnUnLike = functions
  .region("europe-west1")
  .firestore.document("likes/{id}")
  .onDelete((snapshot) => {
    return db
      .doc(`/notifications/${snapshot.id}`)
      .delete()
      .catch((err) => {
        console.error(err);
        return;
      });
  });

// Delete Notification On UnFollow
exports.deleteNotificationOnUnFollow = functions
  .region("europe-west1")
  .firestore.document("follow/{id}")
  .onDelete((snapshot) => {
    return db
      .doc(`/notifications/${snapshot.id}`)
      .delete()
      .catch((err) => {
        console.log(err);
        return;
      });
  });

// Create Notification On Like
exports.createNotificationOnLike = functions
  .region("europe-west1")
  .firestore.document("likes/{id}")
  .onCreate((snapshot) => {
    return db
      .doc(`/cards/${snapshot.data().cardId}`)
      .get()
      .then((doc) => {
        if (
          doc.exists &&
          doc.data().usernameHandle !== snapshot.data().usernameHandle
        ) {
          return db.doc(`/notifications/${snapshot.id}`).set({
            createdAt: new Date().toISOString(),
            recipient: doc.data().usernameHandle,
            sender: snapshot.data().usernameHandle,
            type: "like",
            read: false,
            cardId: doc.id,
          });
        }
      })
      .catch((err) => console.error(err));
  });

// Create Notification On Follow
exports.createNotificationsOnFollow = functions
  .region("europe-west1")
  .firestore.document("follow/{id}")
  .onCreate((snapshot) => {
    return db
      .doc(`/users/${snapshot.data().recipient}`)
      .get()
      .then((doc) => {
        if (doc.exists) {
          return db.doc(`/notifications/${snapshot.id}`).set({
            createdAt: new Date().toISOString(),
            recipient: doc.data().username,
            sender: snapshot.data().sender,
            type: "follow",
            read: false,
          });
        }
      })
      .catch((err) => console.error(err));
  });

// Create Notification On New Post For Followers
exports.createNotificationForFollowersOnNewPost = functions
  .region("europe-west1")
  .firestore.document("cards/{id}")
  .onCreate((snapshot) => {
    return db
      .doc(`/users/${snapshot.data().usernameHandle}`)
      .get()
      .then((doc) => {
        if (doc.exists) {
          doc.data().followersArray.forEach((user) => {
            return db
              .doc(
                `/notifications/${
                  Math.floor(Math.random() * 9999999999) +
                  "" +
                  Math.floor(Math.random() * 9999999999)
                }`
              )
              .set({
                createdAt: new Date().toISOString(),
                recipient: user,
                sender: snapshot.data().usernameHandle,
                type: "post",
                read: false,
                cardId: snapshot.id, //added
              });
          });
        }
      })
      .catch((err) => {
        console.error(err);
        return;
      });
  });

// Create Notification On Comment
exports.createNotificationOnComment = functions
  .region("europe-west1")
  .firestore.document("comments/{id}")
  .onCreate((snapshot) => {
    return db
      .doc(`/cards/${snapshot.data().cardId}`)
      .get()
      .then((doc) => {
        if (
          doc.exists &&
          doc.data().usernameHandle !== snapshot.data().usernameHandle
        ) {
          return db.doc(`/notifications/${snapshot.id}`).set({
            createdAt: new Date().toISOString(),
            recipient: doc.data().usernameHandle,
            sender: snapshot.data().usernameHandle,
            type: "comment",
            read: false,
            cardId: doc.id,
          });
        }
      })
      .catch((err) => {
        console.error(err);
        return;
      });
  });

//  On User Image Change on Cards
exports.onUserImageChange = functions
  .region("europe-west1")
  .firestore.document("/users/{userId}")
  .onUpdate((change) => {
    if (change.before.data().imageUrl !== change.after.data().imageUrl) {
      console.log("Image has changed");
      const batch = db.batch();
      return db
        .collection("cards")
        .where("usernameHandle", "==", change.before.data().username)
        .get()
        .then((data) => {
          data.forEach((doc) => {
            const card = db.doc(`/cards/${doc.id}`);
            batch.update(card, { userImage: change.after.data().imageUrl });
          });
          return batch.commit();
        });
    } else {
      return true;
    }
  });

//  On User Image Change on Comments
exports.onUserImageChangeOnComments = functions
  .region("europe-west1")
  .firestore.document("/users/{userId}")
  .onUpdate((change) => {
    if (change.before.data().imageUrl !== change.after.data().imageUrl) {
      console.log("Image has changed");
      const batch = db.batch();
      return db
        .collection("comments")
        .where("usernameHandle", "==", change.before.data().username)
        .get()
        .then((data) => {
          data.forEach((doc) => {
            const comment = db.doc(`/comments/${doc.id}`);
            batch.update(comment, { userImage: change.after.data().imageUrl });
          });
          return batch.commit();
        });
    } else {
      return true;
    }
  });

// On Card Delete
exports.onCardDelete = functions
  .region("europe-west1")
  .firestore.document("/cards/{cardId}")
  .onDelete((snapshot, context) => {
    const cardId = context.params.cardId;
    const batch = db.batch();
    return db
      .collection("comments")
      .where("cardId", "==", cardId)
      .get()
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(db.doc(`/comments/${doc.id}`));
        });
        return db.collection("likes").where("cardId", "==", cardId).get();
      })
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(db.doc(`/likes/${doc.id}`));
        });
        return db
          .collection("notifications")
          .where("cardId", "==", cardId)
          .get();
      })
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(db.doc(`/notifications/${doc.id}`));
        });
        return batch.commit();
      })
      .catch((err) => console.error(err));
  });
