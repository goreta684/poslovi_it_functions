const { admin, db } = require("../util/admin");

const config = require("../util/config");

const firebase = require("firebase");
firebase.initializeApp(config);

const {
  validateSignupData,
  validateLoginData,
  reduceUserDetails,
} = require("../util/validators");

// Signup User
exports.signup = (req, res) => {
  const newUser = {
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body.confirmPassword,
    username: req.body.username,
    employerName: req.body.employerName,
    role: req.body.role,
  };

  const { valid, errors } = validateSignupData(newUser);
  if (!valid) return res.status(400).json(errors);

  const noImg = "no-img.png";

  let token, userId;
  db.doc(`/users/${newUser.username}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        return res.status(400).json({
          username: "This username is already taken",
        });
      } else {
        return firebase //because we have this block we must return
          .auth()
          .createUserWithEmailAndPassword(newUser.email, newUser.password);
      }
    })
    .then((data) => {
      userId = data.user.uid;
      return data.user.getIdToken();
    })
    .then((idToken) => {
      token = idToken;
      const userCredentials = {
        username: newUser.username,
        employerName: newUser.employerName,
        email: newUser.email,
        createdAt: new Date().toISOString(),
        imageUrl: `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${noImg}?alt=media`,
        role: newUser.role,
        userId,
        followers: 0,
        following: 0,
        followersArray: [],
      };
      return db.doc(`/users/${newUser.username}`).set(userCredentials);
    })
    .then(() => {
      return res.status(201).json({ token });
    })
    .catch((err) => {
      console.error(err);
      if (err.code === "auth/email-already-in-use") {
        return res.status(400).json({
          email: "Email is already in use",
        });
      } else {
        return res
          .status(500)
          .json({ general: "Something went wrong, please try again" });
      }
    });
};

// Login User
exports.login = (req, res) => {
  const user = {
    email: req.body.email,
    password: req.body.password,
  };

  const { valid, errors } = validateLoginData(user);
  if (!valid) return res.status(400).json(errors);

  firebase
    .auth()
    .signInWithEmailAndPassword(user.email, user.password)
    .then((data) => {
      return data.user.getIdToken();
    })
    .then((token) => {
      return res.json({ token });
    })
    .catch((err) => {
      console.error(err);
      if (err.code === "auth/wrong-password") {
        return res
          .status(403)
          .json({ general: "Wrong credentials, please try again" });
      } else if (err.code === "auth/invalid-email") {
        return res.status(400).json({ email: "Invalid email" });
      } else if (err.code === "auth/user-not-found") {
        return res.status(400).json({ email: "User not found" });
      } else return res.status(500).json({ error: err.code });
    });
};

// Add User Details
exports.addUserDetails = (req, res) => {
  let userDetails = reduceUserDetails(req.body);

  db.doc(`/users/${req.user.username}`)
    .update(userDetails)
    .then(() => {
      return res.json({ message: "Details added succesfully" });
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

// GET Any Users's Details
exports.getUserDetails = (req, res) => {
  let userData = {};
  db.doc(`/users/${req.params.username}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        userData.user = doc.data();
        return db
          .collection("cards")
          .where("usernameHandle", "==", req.params.username)
          .orderBy("createdAt", "desc")
          .get();
      } else {
        return res.status(404).json({ error: "User not found" });
      }
    })
    .then((data) => {
      userData.cards = [];
      data.forEach((doc) => {
        userData.cards.push({
          body: doc.data().body,
          createdAt: doc.data().createdAt,
          usernameHandle: doc.data().usernameHandle,
          employerNameHandle: doc.data().employerNameHandle,
          userImage: doc.data().userImage,
          likeCount: doc.data().likeCount,
          commentCount: doc.data().commentCount,
          cardId: doc.id,
        });
      });
      return res.json(userData);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

// Get Own User Details
exports.getAuthenticatedUser = (req, res) => {
  let userData = {};
  db.doc(`/users/${req.user.username}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        userData.credentials = doc.data();
        return db
          .collection("likes")
          .where("usernameHandle", "==", req.user.username)
          .get();
      }
    })
    .then((data) => {
      userData.likes = [];
      data.forEach((doc) => {
        userData.likes.push(doc.data());
      });
      return db
        .collection("notifications")
        .where("recipient", "==", req.user.username)
        .orderBy("createdAt", "desc")
        .limit(10)
        .get();
    })
    .then((data) => {
      userData.notifications = [];
      data.forEach((doc) => {
        userData.notifications.push({
          recipient: doc.data().recipient,
          sender: doc.data().sender,
          createdAt: doc.data().createdAt,
          cardId: doc.data().cardId,
          type: doc.data().type,
          read: doc.data().read,
          notificationId: doc.id,
        });
      });
      return db
        .collection("follow")
        .where("recipient", "==", req.user.username)
        .get();
    })
    .then((data) => {
      userData.followers = [];
      data.forEach((doc) => {
        userData.followers.push({
          //*
          sender: doc.data().sender,
          recipient: req.user.username,
        });
      });
      return db
        .collection("follow")
        .where("sender", "==", req.user.username)
        .get();
    })
    .then((data) => {
      userData.following = [];
      data.forEach((doc) => {
        userData.following.push({
          recipient: doc.data().recipient,
          sender: req.user.username,
        });
      });
      return res.json(userData);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

// Upload Image
exports.uploadImage = (req, res) => {
  const BusBoy = require("busboy");
  const path = require("path");
  const os = require("os");
  const fs = require("fs");

  const busboy = new BusBoy({ headers: req.headers });

  let imageFileName;
  let imageToBeUploaded = {};

  busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
    if (mimetype !== "image/jpeg" && mimetype !== "image/png") {
      return res.status(400).json({ error: "Wrong file type submitted" });
    }
    // my.image.png
    const imageExtension = filename.split(".")[filename.split(".").length - 1];
    // 54354354353.png
    imageFileName = `${Math.round(
      Math.random() * 100000000000
    )}.${imageExtension}`;
    const filepath = path.join(os.tmpdir(), imageFileName);
    imageToBeUploaded = { filepath, mimetype };
    file.pipe(fs.createWriteStream(filepath));
  });
  busboy.on("finish", () => {
    admin
      .storage()
      .bucket()
      .upload(imageToBeUploaded.filepath, {
        resumable: false,
        metadata: {
          metadata: {
            contentType: imageToBeUploaded.mimetype,
          },
        },
      })
      .then(() => {
        const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageFileName}?alt=media`;
        return db.doc(`/users/${req.user.username}`).update({ imageUrl });
      })
      .then(() => {
        return res.json({ message: "Image uploaded successfuly" });
      })
      .catch((err) => {
        console.error(err);
        return res.status(500).json({ error: err.code });
      });
  });
  busboy.end(req.rawBody);
};

// Mark Notification Read
exports.markNotificationsRead = (req, res) => {
  let batch = db.batch();
  req.body.forEach((notificationId) => {
    const notification = db.doc(`/notifications/${notificationId}`);
    batch.update(notification, { read: true });
  });
  batch
    .commit()
    .then(() => {
      return res.json({ message: "Notifications marked read" });
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

// Get All Users
exports.getAllUsers = (req, res) => {
  db.collection("users")
    .orderBy("employerName", "asc")
    .get()
    .then((data) => {
      let users = [];
      data.forEach((doc) => {
        users.push({
          userId: doc.id,
          contact: doc.data().contact,
          createdAt: doc.data().createdAt,
          description: doc.data().description,
          email: doc.data().email,
          employerName: doc.data().employerName,
          imageUrl: doc.data().imageUrl,
          location: doc.data().location,
          website: doc.data().website,
          oib: doc.data().oib,
          role: doc.data().role,
          username: doc.data().username,
          git: doc.data().git,
          linkedin: doc.data().linkedin,
          socialNetwork: doc.data().socialNetwork,
          other: doc.data().other,
          followers: doc.data().followers,
          following: doc.data().following,
        });
      });
      return res.json(users);
    })
    .catch((err) => console.error(err));
};

// GET All Users By Role
exports.getUsersByRole = (req, res) => {
  db.collection("users")
    .orderBy("employerName", "asc")
    .where("role", "==", req.params.role)
    .get()
    .then((data) => {
      let users = [];
      data.forEach((doc) => {
        users.push({
          userId: doc.id,
          contact: doc.data().contact,
          createdAt: doc.data().createdAt,
          description: doc.data().description,
          email: doc.data().email,
          employerName: doc.data().employerName,
          imageUrl: doc.data().imageUrl,
          location: doc.data().location,
          website: doc.data().website,
          oib: doc.data().oib,
          role: doc.data().role,
          username: doc.data().username,
          git: doc.data().git,
          linkedin: doc.data().linkedin,
          socialNetwork: doc.data().socialNetwork,
          other: doc.data().other,
          followers: doc.data().followers,
          following: doc.data().following,
        });
      });
      return res.json(users);
    })
    .catch((err) => console.error(err));
};

// Get Users By Location
exports.getUsersByLocation = (req, res) => {
  db.collection("users")
    .where("location", "==", req.params.location)
    .get()
    .then((data) => {
      let users = [];
      data.forEach((doc) => {
        users.push(doc.data());
      });
      return res.json(users);
    })
    .catch((err) => console.error(err));
};

// Get Users By Email
exports.getUsersByEmail = (req, res) => {
  db.collection("users")
    .where("email", "==", req.params.email)
    .get()
    .then((data) => {
      let users = [];
      data.forEach((doc) => {
        users.push(doc.data());
      });
      return res.json(users);
    })
    .catch((err) => console.error(err));
};

// Follow User
exports.followUser = (req, res) => {
  const followDocument = db
    .collection("follow")
    .where("sender", "==", req.user.username)
    .where("recipient", "==", req.params.username)
    .limit(1);

  const userDocument = db.doc(`/users/${req.params.username}`);
  const senderDocument = db.doc(`/users/${req.user.username}`);

  let userData;
  let senderData;

  senderData = senderDocument.get().then((doc) => {
    if (doc.exists) {
      senderData = doc.data();
    }
  });

  userDocument
    .get()
    .then((doc) => {
      if (doc.exists) {
        userData = doc.data();
        userData.userId = doc.id;
        return followDocument.get();
      } else {
        return res.status(404).json({ error: "User not found" });
      }
    })
    .then((data) => {
      if (data.empty) {
        return db
          .collection("follow")
          .add({ recipient: req.params.username, sender: req.user.username })
          .then(() => {
            userData.followers++;
            senderData.following++;
            senderData.followersArray.push(req.params.username);
            return (
              userDocument.update({
                followers: userData.followers,
              }),
              senderDocument.update({
                following: senderData.following,
                followersArray: senderData.followersArray,
              })
            );
          })
          .then(() => {
            return res.json({ userData, senderData });
          });
      } else {
        return res.status(400).json({ error: "You already follow this user" });
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

//Unfollow User
exports.unfollowUser = (req, res) => {
  let newFollowers = [];
  const followDocument = db
    .collection("follow")
    .where("sender", "==", req.user.username)
    .where("recipient", "==", req.params.username)
    .limit(1);

  const userDocument = db.doc(`/users/${req.params.username}`);
  const senderDocument = db.doc(`/users/${req.user.username}`);

  let userData;
  let senderData;

  senderData = senderDocument.get().then((doc) => {
    if (doc.exists) {
      senderData = doc.data();
    }
  });

  userDocument
    .get()
    .then((doc) => {
      if (doc.exists) {
        userData = doc.data();
        userData.userId = doc.id;
        return followDocument.get();
      } else {
        return res.status(404).json({ error: "User not found" });
      }
    })
    .then((data) => {
      if (data.empty) {
        return res.status(400).json({ error: "User not followed" });
      } else {
        return db
          .doc(`/follow/${data.docs[0].id}`)
          .delete()
          .then(() => {
            userData.followers--;
            senderData.following--;
            senderData.followersArray.filter(function (value) {
              if (value !== req.params.username) {
                newFollowers.push(value);
              }
            });
            return (
              userDocument.update({
                followers: userData.followers,
              }),
              senderDocument.update({
                following: senderData.following,
                followersArray: newFollowers,
              })
            );
          })
          .then(() => {
            return res.json({ userData, senderData });
          });
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

// Get All Followers
exports.getAllFollowers = (req, res) => {
  db.collection("follow")
    .where("recipient", "==", req.params.recipient) //[req.user.username] instead [req.params.recipient]
    .get()
    .then((data) => {
      let followers = [];
      data.forEach((doc) => {
        followers.push({
          followId: doc.id,
          sender: doc.data().sender,
          recipient: req.user.username,
        });
      });
      return res.json(followers);
    })
    .catch((err) => console.error(err));
};

// Get All Followings
exports.getAllFollowing = (req, res) => {
  db.collection("follow")
    .where("sender", "==", req.params.sender)
    .get()
    .then((data) => {
      let following = [];
      data.forEach((doc) => {
        following.push({
          followId: doc.id,
          sender: req.user.username,
          recipient: doc.data().recipient,
        });
      });
      return res.json(following);
    })
    .catch((err) => console.error(err));
};

function matchString(allTechs, paramName) {
  allTechs = allTechs.toLowerCase();
  paramName = paramName.toLowerCase();
  let isIncludes = allTechs.includes(paramName);
  return isIncludes;
}

// Get Users By Tech
exports.getUsersByTech = (req, res) => {
  let paramName = req.params.techname;
  db.collection("users")
    .get()
    .then((data) => {
      let techs = [];
      data.forEach((doc) => {
        if (
          doc.data().other !== undefined &&
          doc.data().other !== "" &&
          matchString(doc.data().other, paramName)
        ) {
          techs.push({
            techName: paramName,
            description: doc.data().description,
            employerName: doc.data().employerName,
            username: doc.data().username,
            role: doc.data().role,
            other: doc.data().other,
          });
        }
      });
      return res.json(techs);
    })
    .catch((err) => console.log(err));
};
