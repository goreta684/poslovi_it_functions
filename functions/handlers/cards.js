const { db } = require("../util/admin");

const { updateDetails } = require("../util/validators");

// GET All Cards
exports.getAllCards = (req, res) => {
  db.collection("cards")
    .orderBy("createdAt", "desc")
    .get()
    .then((data) => {
      let cards = [];
      data.forEach((doc) => {
        cards.push({
          cardId: doc.id,
          body: doc.data().body,
          usernameHandle: doc.data().usernameHandle,
          employerNameHandle: doc.data().employerNameHandle,
          createdAt: doc.data().createdAt,
          commentCount: doc.data().commentCount,
          likeCount: doc.data().likeCount,
          userImage: doc.data().userImage,
          updatedTime: doc.data().updatedTime, //
        });
      });
      return res.json(cards);
    })
    .catch((err) => console.error(err));
};

// POST Card
exports.postOneCard = (req, res) => {
  if (req.method !== "POST") {
    return res.status(400).json({ error: "Method not allowed." });
  }
  if (req.body.body.trim() === "") {
    return res.status(400).json({ body: "Must not be empty" });
  }
  const newCard = {
    body: req.body.body,
    usernameHandle: req.user.username,
    employerNameHandle: req.user.employerName,
    userImage: req.user.imageUrl,
    createdAt: new Date().toISOString(),
    likeCount: 0,
    commentCount: 0,
  };

  db.collection("cards")
    .add(newCard)
    .then((doc) => {
      const resCard = newCard;
      resCard.cardId = doc.id;
      res.json(resCard);
    })
    .catch((err) => {
      res.status(500).json({ error: `Something went wrong. ` });
    });
};

// Get One Card And His Comments
exports.getCard = (req, res) => {
  let adData = {};
  db.doc(`/cards/${req.params.cardId}`)
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Card not found" });
      }
      adData = doc.data();
      adData.cardId = doc.id;
      return db
        .collection("comments")
        .orderBy("createdAt", "desc")
        .where("cardId", "==", req.params.cardId)
        .get();
    })
    .then((data) => {
      adData.comments = [];
      data.forEach((doc) => {
        adData.comments.push({
          employerNameHandle: doc.data().employerNameHandle,
          createdAt: doc.data().createdAt,
          userImage: doc.data().userImage,
          usernameHandle: doc.data().usernameHandle,
          body: doc.data().body,
          cardId: doc.data().cardId,
          commentId: doc.id,
          updatedTime: doc.data().updatedTime,
        });
      });
      return res.json(adData);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

// Comment on Card
exports.commentOnCard = (req, res) => {
  if (req.body.body.trim() === "")
    return res.status(400).json({ comment: "Must not be empty" });

  const newComment = {
    body: req.body.body,
    createdAt: new Date().toISOString(),
    cardId: req.params.cardId,
    usernameHandle: req.user.username,
    employerNameHandle: req.user.employerName,
    userImage: req.user.imageUrl,
    commCount: 0,
  };

  let commCounter;

  db.doc(`/cards/${req.params.cardId}`)
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Card not found" });
      }
      commCounter = doc.data().commentCount + 1;
      return doc.ref.update({ commentCount: doc.data().commentCount + 1 });
    })
    .then(() => {
      return db.collection("comments").add(newComment);
    })
    .then(() => {
      newComment.commCount = commCounter;
    })
    .then(() => {
      res.json(newComment);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: "Something went wrong" });
    });
};

// Like Card
exports.likeCard = (req, res) => {
  const likeDocument = db
    .collection("likes")
    .where("usernameHandle", "==", req.user.username)
    .where("cardId", "==", req.params.cardId)
    .limit(1);

  const adDocument = db.doc(`/cards/${req.params.cardId}`);

  let adData;

  adDocument
    .get()
    .then((doc) => {
      if (doc.exists) {
        adData = doc.data();
        adData.cardId = doc.id;
        return likeDocument.get();
      } else {
        return res.status(404).json({ error: "Card not found" });
      }
    })
    .then((data) => {
      if (data.empty) {
        return db
          .collection("likes")
          .add({
            cardId: req.params.cardId,
            usernameHandle: req.user.username,
          })
          .then(() => {
            adData.likeCount++;
            return adDocument.update({ likeCount: adData.likeCount });
          })
          .then(() => {
            return res.json(adData);
          });
      } else {
        return res.status(400).json({ error: "Card already liked" });
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

// Unlike Card
exports.unlikeCard = (req, res) => {
  const likeDocument = db
    .collection("likes")
    .where("usernameHandle", "==", req.user.username)
    .where("cardId", "==", req.params.cardId)
    .limit(1);

  const adDocument = db.doc(`/cards/${req.params.cardId}`);

  let adData;

  adDocument
    .get()
    .then((doc) => {
      if (doc.exists) {
        adData = doc.data();
        adData.cardId = doc.id;
        return likeDocument.get();
      } else {
        return res.status(404).json({ error: "Card not found" });
      }
    })
    .then((data) => {
      if (data.empty) {
        return res.status(400).json({ error: "Card not liked" });
      } else {
        return db
          .doc(`/likes/${data.docs[0].id}`)
          .delete()
          .then(() => {
            adData.likeCount--;
            return adDocument.update({ likeCount: adData.likeCount });
          })
          .then(() => {
            res.json(adData);
          });
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

// Delete Card
exports.deleteCard = (req, res) => {
  const document = db.doc(`/cards/${req.params.cardId}`);
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Card not found" });
      }
      if (doc.data().usernameHandle !== req.user.username) {
        return res.status(403).json({ error: "Unauthorized" });
      } else {
        return document.delete();
      }
    })
    .then(() => {
      res.json({ message: "Card deleted succesfully" });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({ error: err.code });
    });
};

// Delete Comment
exports.deleteComment = (req, res) => {
  const cardDocument = db.doc(`/cards/${req.params.cardId}`);
  let cardData;
  cardData = cardDocument.get().then((doc) => {
    if (doc.exists) {
      cardData = doc.data();
    }
  });
  const document = db.doc(`/comments/${req.params.commentId}`);
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Comment not found" });
      }
      if (doc.data().usernameHandle !== req.user.username) {
        return res.status(403).json({ error: "Unauthorized" });
      } else {
        cardData.commentCount--;
        if (cardData.commentCount >= 0) {
          return (
            document.delete(),
            cardDocument.update({ commentCount: cardData.commentCount })
          );
        } else {
          return res.status(404).json({ error: "Comment not found" });
        }
      }
    })
    .then(() => {
      res.json(cardData.commentCount);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({ error: err.code });
    });
};

// Edit Post
exports.editPost = (req, res) => {
  let postDetails = updateDetails(req.body);
  db.collection("cards")
    .doc(`${req.params.cardId}`)
    .update(postDetails)
    .then(() => {
      return res.json(postDetails);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

// Edit Comment
exports.editComment = (req, res) => {
  let commentDetails = updateDetails(req.body);
  db.collection("comments")
    .doc(`${req.params.commentId}`)
    .update(commentDetails)
    .then(() => {
      return res.json(commentDetails);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};
