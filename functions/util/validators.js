// Function for validate email
const isEmail = (email) => {
  const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (email.match(regEx)) return true;
  else return false;
};

// Function for validate entry
const isEmpty = (string) => {
  if (string.trim() === "") return true;
  else return false;
};

exports.validateSignupData = (data) => {
  let errors = {};

  if (isEmpty(data.email)) {
    errors.email = "Must not be empty";
  } else if (!isEmail(data.email)) {
    errors.email = "Must be a valid email address";
  }

  if (isEmpty(data.password)) errors.password = "Must not be empty";
  if (data.password !== data.confirmPassword)
    errors.confirmPassword = "Passwords must match";

  if (isEmpty(data.username)) errors.username = "Must not be empty";
  if (isEmpty(data.employerName)) errors.employerName = "Must not be empty";

  return {
    errors,
    valid: Object.keys(errors).length === 0 ? true : false,
  };
};

exports.validateLoginData = (data) => {
  let errors = {};

  if (isEmpty(data.email)) errors.email = "Must not be empty";
  if (isEmpty(data.password)) errors.password = "Must not be empty";

  return {
    errors,
    valid: Object.keys(errors).length === 0 ? true : false,
  };
};

exports.reduceUserDetails = (data) => {
  let userDetails = {};
  if (!isEmpty(data.description.trim()))
    userDetails.description = data.description;
  if (!isEmpty(data.website.trim())) {
    if (data.website.trim().substring(0, 4) !== "http") {
      userDetails.website = `http://${data.website.trim()}`;
    } else userDetails.website = data.website;
  }
  if (!isEmpty(data.location.trim())) userDetails.location = data.location;

  if (!isEmpty(data.contact.trim())) userDetails.contact = data.contact;

  if (!isEmpty(data.git.trim())) userDetails.git = data.git;

  if (!isEmpty(data.linkedin.trim())) userDetails.linkedin = data.linkedin;

  if (!isEmpty(data.socialNetwork.trim()))
    userDetails.socialNetwork = data.socialNetwork;

  if (!isEmpty(data.other.trim())) userDetails.other = data.other;

  // Change oib logic if length != 11
  if (!isEmpty(data.oib.trim())) {
    if (data.oib.trim().length == 11) {
      userDetails.oib = data.oib;
    } else {
      userDetails.oib = "0";
    }
  }
  return userDetails;
};

// UPDATE card or comment
exports.updateDetails = (data) => {
  let detailsForUpdate = {};
  if (!isEmpty(data.body.trim())) detailsForUpdate.body = data.body;
  detailsForUpdate.updatedTime = new Date().toISOString();
  return detailsForUpdate;
};
