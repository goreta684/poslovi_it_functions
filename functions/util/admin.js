const admin = require("firebase-admin");

var serviceAccount = require("../poslovi-it-742500bfdf3f.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://poslovi-it.firebaseio.com",
  storageBucket: "poslovi-it.appspot.com",
});

const db = admin.firestore();

module.exports = { admin, db };
